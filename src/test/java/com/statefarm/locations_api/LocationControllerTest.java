package com.statefarm.locations_api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static com.statefarm.locations_api.LocationTestUtilities.testLocation;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LocationController.class)
public class LocationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    LocationService locationService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void getLocations_noParams_exists_returnsLocationList() throws Exception {
        List<Location> locations = new ArrayList<>();

        mockMvc.perform(get("/api/locations"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.locations", hasSize(0)));
    }

    @Test
    void postLocation_withFields_andReturnLocation() throws Exception {
        Location location = testLocation();
        when(locationService.addLocation(any(Location.class))).thenReturn(location);
        mockMvc.perform(post("/api/locations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(location))
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("New York"))
                .andExpect(jsonPath("$.biome").value("City"));
        verify(locationService).addLocation(any(Location.class));
    }

    @Test
    void getLocationWithRecordShouldReturnLocations() throws Exception {
        Location loco = testLocation();

        when(locationService.getLocations()).thenReturn(List.of(loco));

        mockMvc.perform(get("/api/locations"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.locations", hasSize(1)))
                .andExpect(jsonPath("$.locations[0].name").value(loco.getName()));
    }

    @Test
    void patchLocationWithJsonObjAndReturnsUpdatedLocation() throws Exception {
        Location location = testLocation();
        String lin = location.getLin();
        String resourceURL = "/api/locations/" + lin;
        when(locationService.updateLocation(anyString(), any(UpdateLocationRequest.class))).thenReturn(location);

        mockMvc.perform(patch(resourceURL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"The Haunted Trail\", \"biome\": \"Mountain\"}")
                ).andExpect(status().isOk());
        verify(locationService).updateLocation(anyString(), any(UpdateLocationRequest.class));
    }

    @Test
    void patchLocationWithPlayerAndReturnsUpdatedLocation() throws Exception {
        Player player1 = new Player("Wil Wheaton", "ABC123");

        Location location = testLocation();
        String lin = location.getLin();
        String resourceURL = "/api/locations/" + lin;

        ObjectMapper objectMapper = new ObjectMapper();
        UpdateLocationRequest updateLocationRequest = new UpdateLocationRequest();
        updateLocationRequest.setPlayers(List.of(player1));
        String updateBody = objectMapper.writeValueAsString(updateLocationRequest);

        when(locationService.updateLocation(anyString(), any(UpdateLocationRequest.class))).thenReturn(location);
        mockMvc.perform(patch(resourceURL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateBody))
                .andExpect(status().isOk());
        verify(locationService).updateLocation(anyString(), any(UpdateLocationRequest.class));
    }
}























