package com.statefarm.locations_api;

import java.util.ArrayList;

public class LocationTestUtilities {
    public static Location testLocation() {
        Location location = new Location();
        location.setName("New York");
        location.setBiome("City");
        location.setLatitude(111.111F);
        location.setLongitude(111.111F);
        location.setLin("ABC123");
        location.setPlayers(new ArrayList<>());
        return location;
    }

    public static String testLocationJson() {
        return "{ \"name\": \"New York\", \"biome\": \"City\", \"latitude\": \"111.111\", \"longitude\": \"111.111\", \"lin\": \"ABC123\", \"players\": []}";
    }
}