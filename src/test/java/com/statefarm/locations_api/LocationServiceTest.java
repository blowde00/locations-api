package com.statefarm.locations_api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.statefarm.locations_api.LocationTestUtilities.testLocation;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LocationServiceTest {

    @Mock
    LocationRepository locationRepository;

    LocationService locationService;

    @BeforeEach
    void setup() {
        locationService = new LocationService(locationRepository);
    }

    @Test
    void addLocation_usingService_callsSave() {
        Location location = testLocation();
        when(locationRepository.save(any(Location.class))).thenReturn(location);
        Location result = locationService.addLocation(location);
        assertThat(result.getName()).isEqualTo("New York");
        assertThat(result.getBiome()).isEqualTo("City");
        assertThat(result.getLatitude()).isEqualTo(111.111F);
        assertThat(result.getLongitude()).isEqualTo(111.111F);
    }

    @Test
    void getLocationsReturnsLocationsFromRepository() {
        Location location = testLocation();
        when(locationRepository.findAll()).thenReturn(List.of(location));
        List<Location> result = locationService.getLocations();
        assertThat(result.get(0).getName()).isEqualTo("New York");
        assertThat(result.get(0).getBiome()).isEqualTo("City");
    }

    @Test
    void patchLocationWithNameAndBiomeReturnsLocation() {
        Location location = testLocation();
        when(locationRepository.findByLin(anyString()))
                .thenReturn(Optional.of(location));
        when(locationRepository.save(any(Location.class))).thenReturn(location);
        UpdateLocationRequest uLR = new UpdateLocationRequest();
        uLR.setName("The Haunted Trail");
        uLR.setBiome("Mountain");
        Location updatedLocation = locationService.updateLocation(location.getLin(), uLR);
        assertThat(updatedLocation).isNotNull();
        assertThat(location.getLin()).isEqualTo(location.getLin());
        assertThat(location.getName()).isEqualTo(location.getName());
        assertThat(location.getBiome()).isEqualTo(location.getBiome());
    }
}