package com.statefarm.locations_api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static com.statefarm.locations_api.LocationTestUtilities.testLocationJson;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LocationIntegrationTests {

    @Autowired
    TestRestTemplate restTemplate;
    RestTemplate patchRestTemplate;


    Random r = new Random();
    List<Location> testLocations;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    PlayerRepository playerRepository;

    @BeforeEach
    void setup() {
        this.testLocations = new ArrayList<>();

        this.patchRestTemplate = restTemplate.getRestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        this.patchRestTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

        String[] names = {"RED", "BLUE", "GREEN", "PURPLE", "YELLOW"};
        String[] biomes = {"City", "Forest", "Mountain", "Water", "Desert"};
        Float[] latitudes = {111.111F, 222.222F, 333.333F, 444.444F, 555.555F, 666.666F, 777.777F, 888.888F, 999.999F};
        Float[] longitudes = {111.111F, 222.222F, 333.333F, 444.444F, 555.555F, 666.666F, 777.777F, 888.888F, 999.999F};
        HashMap<String, String> linPrefix = new HashMap<>();
        linPrefix.put(names[0], "RED-01");
        linPrefix.put(names[1], "BLU-02");
        linPrefix.put(names[2], "GRE-03");
        linPrefix.put(names[3], "PUR-04");
        linPrefix.put(names[4], "YEL-05");
        List<Player> testPlayers = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            int nextNameIndex = r.nextInt(names.length);
            int nextBiomeIndex = r.nextInt(biomes.length);
            int nextLatIndex = r.nextInt(latitudes.length);
            int nextLongIndex = r.nextInt(longitudes.length);
            String nextName = names[nextNameIndex];
            String nextBiome = biomes[nextBiomeIndex];
            float nextLat = latitudes[nextLatIndex];
            float nextLong = longitudes[nextLongIndex];
            String nextLin = String.format("%s%5s", linPrefix.get(nextName), i).replaceAll(" ", "0");
            Location loca = new Location(nextName, nextBiome, nextLat, nextLong, nextLin, testPlayers);

            this.testLocations.add(loca);
        }
        locationRepository.saveAll(this.testLocations);
    }

    @AfterEach
    void teardown() {
        locationRepository.deleteAll();
    }

    public HttpEntity getPostRequestHeaders(String jsonPostBody) {
        List acceptTypes = new ArrayList();
        acceptTypes.add(MediaType.APPLICATION_JSON);

        HttpHeaders reqHeaders = new HttpHeaders();
        reqHeaders.setContentType(MediaType.APPLICATION_JSON);
        reqHeaders.setAccept(acceptTypes);

        return new HttpEntity(jsonPostBody, reqHeaders);
    }


    @Test
    void contextLoads() {
    }

    @Test
    void getLocations_returnsLocationList() {
        ResponseEntity<LocationList> response = restTemplate.getForEntity("/api/locations", LocationList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        LocationList locationList = response.getBody();
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
    }

    @Test
        //	GIVEN The consumer has a location that does not exist
        //	WHEN they provide the location details
        //	THEN the location should be saved and returned
    void postALocation() {
        String url = "/api/locations";
        String locationJson = testLocationJson();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> httpEntityReq = new HttpEntity<>(locationJson, httpHeaders);

        ResponseEntity<Location> postResponse = restTemplate.postForEntity(url, httpEntityReq, Location.class);
        assertThat(postResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        Location responseLocation = postResponse.getBody();

        assertThat(responseLocation.getName()).isEqualTo("New York");
        assertThat(responseLocation.getBiome()).isEqualTo("City");
    }

    @Test
    void postLocationThenGetLocation() {
        String url = "/api/locations";
        String locationJson = testLocationJson();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> httpEntityReq = new HttpEntity<>(locationJson, httpHeaders);

        ResponseEntity<Location> postResponse = restTemplate.postForEntity(url, httpEntityReq, Location.class);
        assertThat(postResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        Location responseLocation = postResponse.getBody();

        ResponseEntity<LocationList> getResponse = restTemplate.getForEntity(url, LocationList.class);
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        LocationList locationList = getResponse.getBody();
        assertThat(locationList.getLocations().isEmpty()).isFalse();
        Optional<Location> returnedLocation = locationRepository.findByLin("ABC123");
        assertThat(responseLocation.getName()).isEqualTo(returnedLocation.get().getName());
        assertThat(responseLocation.getBiome()).isEqualTo(returnedLocation.get().getBiome());
        assertThat(responseLocation.getLin()).isEqualTo(returnedLocation.get().getLin());
    }

    @Test
//	GIVEN The consumer has a location that already exists
//	WHEN they provide the location details with an identifier
//	THEN the location should be updated and returned
    void patchNameAndBiomeWithLinReturnsUpdatedLocation() throws JSONException {
        int seq = r.nextInt(50);
        String lin = testLocations.get(seq).getLin();
        String resourceURL = "/api/locations/" + lin;

        JSONObject updateBody = new JSONObject();
        updateBody.put("name", "The Haunted Trail");
        updateBody.put("biome", "Mountain");

        // Act
        ResponseEntity<Location> responseEntity = patchRestTemplate.exchange(resourceURL, HttpMethod.PATCH, getPostRequestHeaders(updateBody.toString()), Location.class);

        // Assert
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        //// Ensure Content-Type is application/json
        assertThat(responseEntity.getHeaders().getContentType().toString()).isEqualTo(MediaType.APPLICATION_JSON_VALUE);
        //// Ensure that POST updated name and biome
        assertThat(responseEntity.getBody().getName()).isEqualTo("The Haunted Trail");
        assertThat(responseEntity.getBody().getBiome()).isEqualTo("Mountain");
    }

    @Test
        //	GIVEN The consumer has a character name to assign
        //	WHEN they provide the name and a location identifier
        //	THEN the character name should be associated with that location
    void patchPlayerIntoLocationReturnsUpdatedLocationWithPlayer() throws JSONException, JsonProcessingException {
        Player player1 = new Player("Wil Wheaton", "ABC123");
        playerRepository.save(player1);

        int seq = r.nextInt(50);
        String lin = testLocations.get(seq).getLin();
        String resourceURL = "/api/locations/" + lin;

        ObjectMapper objectMapper = new ObjectMapper();
        UpdateLocationRequest updateLocationRequest = new UpdateLocationRequest();
        updateLocationRequest.setPlayers(List.of(player1));
        String updateBody = objectMapper.writeValueAsString(updateLocationRequest);

        // Act
        ResponseEntity<Location> responseEntity = patchRestTemplate.exchange(resourceURL, HttpMethod.PATCH, getPostRequestHeaders(updateBody.toString()), Location.class);
        // Assert
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        // Ensure Content-Type is application/json
        assertThat(responseEntity.getHeaders().getContentType().toString()).isEqualTo(MediaType.APPLICATION_JSON_VALUE);
        // Ensure that POST updated player
        assertThat(responseEntity.getBody().getPlayers().get(0).getName()).isEqualTo("Wil Wheaton");
        assertThat(responseEntity.getBody().getPlayers().get(0).getPin()).isEqualTo("ABC123");

        // Ensure that the location name and biome are not overwritten to null
        assertThat(responseEntity.getBody().getBiome()).isNotNull();
        assertThat(responseEntity.getBody().getName()).isNotNull();

        System.out.println(responseEntity.getBody());
    }


//	GIVEN The consumer has provided location data previously
//	WHEN they request information about that location
//	THEN the location data should be provided, including any associated characters


//	GIVEN A location has a character assigned
//	WHEN that character is no longer needed at that location
//	THEN the character should be able to be removed


//	GIVEN A location has a character assigned
//	WHEN that character is needed at another location
//	THEN the character should be able to be moved to the other location

}
