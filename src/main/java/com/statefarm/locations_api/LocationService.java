package com.statefarm.locations_api;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class LocationService {
    LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public Location addLocation(Location location) {
        return locationRepository.save(location);
    }

    public List<Location> getLocations() {
        return locationRepository.findAll();
    }

    public Location updateLocation(String lin, UpdateLocationRequest updateLocationRequest) {
        Optional<Location> optionalLocation = locationRepository.findByLin(lin);
        if (optionalLocation.isPresent()) {
            String reqName = updateLocationRequest.getName();
            String reqBiome = updateLocationRequest.getBiome();
            List<Player> reqPlayers = updateLocationRequest.getPlayers();

            if (reqName != null) {
                optionalLocation.get().setName(reqName);
            }

            if (reqBiome != null) {
                optionalLocation.get().setBiome(reqBiome);
            }

            if (reqPlayers != null) {
                optionalLocation.get().getPlayers().addAll(reqPlayers);
            }

            return locationRepository.save(optionalLocation.get());
        }
        return null;
    }
}