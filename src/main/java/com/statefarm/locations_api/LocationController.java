package com.statefarm.locations_api;

import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {
    LocationService locationService;

    LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/api/locations")
    public LocationList getLocations() {
        LocationList locationList = new LocationList();
        locationList.setLocations(locationService.getLocations());
        return locationList;
    }

    @PostMapping("/api/locations")
    public Location postLocation(@RequestBody Location location) {
        Location result = locationService.addLocation(location);
        return result;
    }

    @PatchMapping("api/locations/{lin}")
    public Location updateLocation(@PathVariable String lin, @RequestBody UpdateLocationRequest updateLocationRequest) {
        Location location = locationService.updateLocation(lin, updateLocationRequest);
        return location;
    }
}
