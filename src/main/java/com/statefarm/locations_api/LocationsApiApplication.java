package com.statefarm.locations_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocationsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocationsApiApplication.class, args);
	}

}
