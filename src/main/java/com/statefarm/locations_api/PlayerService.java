package com.statefarm.locations_api;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerService {
    PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public Player addPlayer(Player player) {
        return playerRepository.save(player);
    }

    public List<Player> getPlayers() {
        return playerRepository.findAll();
    }

    public Player updatePlayer(String pin, String name) {
        Optional<Player> optionalPlayer = playerRepository.findByPin(pin);
        if(optionalPlayer.isPresent()) {
            optionalPlayer.get().setName(name);
            return playerRepository.save(optionalPlayer.get());
        }
        return null;
    }
}
