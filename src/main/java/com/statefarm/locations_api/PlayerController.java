package com.statefarm.locations_api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlayerController {
    PlayerService playerService;

    PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping("/api/players")
    public PlayerList getPlayers() {
        PlayerList playerList = new PlayerList();
        playerList.setPlayers(playerService.getPlayers());
        return playerList;
    }

    @PostMapping("/api/players")
    public Player postLocation(@RequestBody Player player) {
        Player result = playerService.addPlayer(player);
        return result;
    }
}
