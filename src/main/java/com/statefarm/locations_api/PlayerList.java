package com.statefarm.locations_api;

import java.util.ArrayList;
import java.util.List;

public class PlayerList {
    private List<Player> players;

    public PlayerList() {
        this.players = new ArrayList<>();
    }

    public PlayerList(List<Player> players) {
        this.players = players;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public boolean isEmpty() {
        return this.players.isEmpty();
    }
}
