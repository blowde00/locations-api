package com.statefarm.locations_api;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String biome;
    private float latitude;
    private float longitude;
    private String lin;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Player> players;

    public Location(String name, String biome, float latitude, float longitude, String lin, List<Player> players) {
        this.name = name;
        this.biome = biome;
        this.latitude = latitude;
        this.longitude = longitude;
        this.lin = lin;
        this.players = players;
    }

    public Location() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBiome() {
        return biome;
    }

    public void setBiome(String biome) {
        this.biome = biome;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLin() {
        return lin;
    }

    public void setLin(String lin) {
        this.lin = lin;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Float.compare(location.latitude, latitude) == 0 && Float.compare(location.longitude, longitude) == 0 && Objects.equals(id, location.id) && Objects.equals(name, location.name) && Objects.equals(biome, location.biome) && Objects.equals(lin, location.lin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, biome, latitude, longitude, lin);
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", biome='" + biome + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", lin='" + lin + '\'' +
                '}';
    }
}
